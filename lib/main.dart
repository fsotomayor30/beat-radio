import 'dart:async';

import 'package:flutter/material.dart';
import 'package:beat_radio/Module/Menu/Menu_list_view.dart';
import 'package:splashscreen/splashscreen.dart';

import 'package:beat_radio/Module/Inicio.dart';
typedef void OnError(Exception exception);


Future main() async {
  runApp(new MaterialApp(
    home: new Inicio(),
  ));
}

class Inicio extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}
class _MyAppState extends State<Inicio> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 4,
        navigateAfterSeconds: new MenusPage(),
        image: new Image.asset("assets/Imagenes/logofondonegro.png"),
        backgroundColor: Colors.black,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 120,
        //onClick: ()=>print("Flutter Egypt"),
        loaderColor: Colors.white
    );
  }
}
