import 'dart:io';
import 'package:audioplayers/audioplayers.dart';
import 'package:beat_radio/Module/Reproductor_Android/ReproductorAndroid.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:beat_radio/Module/ReproductorIOS/Reproductor.dart';
typedef void OnError(Exception exception);

class ExampleApp extends StatefulWidget {
  @override
  _ExampleAppState createState() => new _ExampleAppState();
}

class _ExampleAppState extends State<ExampleApp> {
  @override
    Widget build(BuildContext context) {

    Widget Radio;
    final mediaQueryData = MediaQuery.of(context).size;
    Widget w_eventos=
    StreamBuilder(
        stream: Firestore.instance.collection("Publicidad").snapshots(),
        builder: (context, snapshot){
          if(!snapshot.hasData) return Container();
          return
            Container(
              decoration: new BoxDecoration(
                  color: Color.fromRGBO(0,0,0,1)
              ),
              width: mediaQueryData.width+0.7,
              child:Column(
                children: <Widget>[
                  CarouselSlider(
                    autoPlayInterval: Duration(seconds: 5),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    autoPlay: true,
                    items: snapshot.data.documents.map<Widget>((DocumentSnapshot document) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: 2,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                              child: Image.network(document['url'])
                            ),
                          );
                        },
                      );
                    }).toList(),
                  ),
                ],
              ),
            );

        });

        Radio =SingleChildScrollView(
            child: Stack(
                children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                  color: Color.fromRGBO(0,0,0,1)
              ),
              height: mediaQueryData.height,
              child: Column(
              children: <Widget>[
                //PlayerWidget(),
                ReproductorAndroid(),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: w_eventos,
                ),

              ],

        ),
            )
      ]
            )
        );

    return Radio;



  }


  }





