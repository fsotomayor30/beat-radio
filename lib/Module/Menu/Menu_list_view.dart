import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:beat_radio/Module/Inicio.dart';
import 'package:beat_radio/Module/Clientes/Clientes.dart';
import 'package:provider/provider.dart';//es la dependencia que importamos.
import 'package:beat_radio/Module/Provider/Android.dart'; //es el archivo que creamos
import 'package:beat_radio/Module/Provider/IOS.dart'; //es el archivo que creamos
import 'package:beat_radio/Module/Video/ReproductorVideo.dart';
import 'dart:io';

class MenusPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Widget e;
    if(Platform.isAndroid){

      //e= Container();


        e=ChangeNotifierProvider<AndroidProvider>(
          builder: (context) => AndroidProvider() ,
          child: Scaffold(
            body: MenuList(),
          ),
        );


    }else{
      if(Platform.isIOS){

        e= Container();

/*        e=ChangeNotifierProvider<IOSProvider>(
          builder: (context) => IOSProvider() ,
          child: Scaffold(
            body: MenuList(),
          ),
        );*/
      }
    }

    return e;
  }
}

class MenuList extends StatelessWidget{


  List<Widget> containers = [
    Container(
        child: ExampleApp()
    ),
    Container(
      child: VideoExample(),
    ),
    Container(
      child: Auspiciadores(),
    )
  ];



  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0,0,0,1),

          title: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Image.asset('assets/Imagenes/logofondonegro.png'),
              ),
              // Text('iChillán'),
            ],
          ),
          bottom : TabBar(indicatorColor: Colors.white,tabs: <Widget>[
            Tab(
                icon: Icon(Icons.audiotrack),
                text: 'Audio'
            ),
            Tab(
              icon: Icon(Icons.play_arrow),
                text: 'Video'
            ),
            Tab(
              icon: Icon(Icons.group),
                text: 'Clientes'
            )
          ]),
        ),
        body: TabBarView(children: containers),
        drawer: Drawer(
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(0,0,0,1),
            ),
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                  child: Image.asset('assets/Imagenes/logofondonegro.png', width: 10,),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(0,0,0,1),
                  ),
                ),

                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(0,0,0,1),
                  ),
                  child: ListTile(
                    title: Text('Sitio Web Beat Radio', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/www.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () async {
                      if (await canLaunch("http://beatradio.cl/")) {
                        await launch("http://beatradio.cl/");
                      }
                    },
                  ),
                ),

                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(0,0,0,1),
                  ),
                  child: ListTile(
                    title: Text('Facebook', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/facebook.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () async {
                      if (await canLaunch("https://www.facebook.com/beatradio.cl/")) {
                        await launch("https://www.facebook.com/beatradio.cl/");
                      }
                    },
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(0,0,0,1),
                  ),
                  child: ListTile(
                    title: Text('Instagram', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/instagram.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () async {
                      if (await canLaunch("https://www.instagram.com/Beatradio_chillan/")) {
                        await launch("https://www.instagram.com/Beatradio_chillan/");
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



