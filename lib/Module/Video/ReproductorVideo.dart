import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:provider/provider.dart';
import 'package:beat_radio/Module/Provider/Android.dart';
import 'package:beat_radio/Module/Provider/IOS.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class VideoExample extends StatefulWidget {
  @override
  VideoState createState() => VideoState();
}

class VideoState extends State<VideoExample> {
  VideoPlayerController playerController;
  VoidCallback listener;

  @override
  void initState() {
    super.initState();
    listener = () {
      setState(() {});
    };
  }

  void createVideo() {
    if (playerController == null) {
      playerController = VideoPlayerController.network(
          "http://ad.mysisli.com/live/beatradio/index.m3u8")
        ..addListener(listener)
        ..setVolume(1.0)
        ..initialize()
        ..play();
    } else {
      if (playerController.value.isPlaying) {
        playerController.pause();
      } else {
        playerController.initialize();
        playerController.play();
      }
    }
  }

  @override
  void dispose() {
    if (playerController != null) {
      playerController.setVolume(0.0);
      playerController.removeListener(listener);
    }
    super.dispose();
  }

  @override
  void deactivate() {
    if (playerController != null) {
      playerController.setVolume(0.0);
      playerController.removeListener(listener);
    }
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final test = Provider.of<AndroidProvider>(context);
    Widget Radio, Button;
    if (playerController==null) {
      Button=Row(mainAxisSize: MainAxisSize.min, children: [
        Text("Pulsa Play para iniciar", style: TextStyle(color: Colors.white)),

        new RawMaterialButton(
          onPressed: () {
            createVideo();
            playerController.play();
            test.escuchando ? test.stop() : null;
          },
          child: new Icon(
            Icons.play_arrow,
            color: Colors.blue,
            size: 35.0,
          ),
          shape: new CircleBorder(),
          elevation: 2.0,
          fillColor: Colors.white,
          padding: const EdgeInsets.all(15.0),
        )
      ]);
    } else {

      Button=Row(mainAxisSize: MainAxisSize.min, children: [
          Text("Pulsa Stop para detener", style: TextStyle(color: Colors.white)),
          new RawMaterialButton(
          onPressed: () {
            if (playerController.value.isPlaying) {
              playerController.setVolume(0.0);
              playerController.removeListener(listener);
              playerController = null;
            } else {
              null;
            }
          },
          child: new Icon(
            Icons.stop,
            color: Colors.blue,
            size: 35.0,
          ),
          shape: new CircleBorder(),
          elevation: 2.0,
          fillColor: Colors.white,
          padding: const EdgeInsets.all(15.0),
        )
      ]);
    }
    final mediaQueryData = MediaQuery.of(context).size;
    Widget w_eventos = StreamBuilder(
        stream: Firestore.instance.collection("Publicidad").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container();
          return Container(
            decoration: new BoxDecoration(color: Color.fromRGBO(0, 0, 0, 1)),
            width: mediaQueryData.width + 0.7,
            child: Column(
              children: <Widget>[
                CarouselSlider(
                  autoPlayInterval: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlay: true,
                  items: snapshot.data.documents
                      .map<Widget>((DocumentSnapshot document) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          height: 2,
                          margin: EdgeInsets.symmetric(horizontal: 5.0),
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                              child: Image.network(document['url'])),
                        );
                      },
                    );
                  }).toList(),
                ),
              ],
            ),
          );
        });

    Radio = SingleChildScrollView(
        child: Stack(children: <Widget>[
      Container(
        decoration: new BoxDecoration(color: Color.fromRGBO(0, 0, 0, 1)),
        height: mediaQueryData.height,
        child: Column(
          children: <Widget>[
            Center(
                child: AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Container(
                      child: (playerController != null
                          ? VideoPlayer(
                              playerController,
                            )
                          : Image.asset("assets/Imagenes/logofondonegro.png")),
                    ))),
            Button,
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: w_eventos,
            ),
          ],
        ),
      )
    ]));

    return Radio;
  }
}
