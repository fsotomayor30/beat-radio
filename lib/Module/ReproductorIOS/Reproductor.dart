import 'dart:async';

import 'package:flutter/material.dart';
import 'package:beat_radio/Module/Provider/IOS.dart';
import 'package:provider/provider.dart';

class PlayerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final test = Provider.of<IOSProvider>(context);
    Widget button;
    if (!test.escuchando) {
      button = Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("Pulsa Play para iniciar", style: TextStyle(color: Colors.white)),

          RawMaterialButton(
            onPressed: () => test.play(),
            child: new Icon(
              Icons.play_arrow,
              color: Colors.blue,
              size: 35.0,
            ),
            shape: new CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.white,
            padding: const EdgeInsets.all(15.0),
          ),
        ],
      );
    } else {
      button=Row(mainAxisSize: MainAxisSize.min, children: [
        Text("Pulsa Stop para detener", style: TextStyle(color: Colors.white)
        ),
        new RawMaterialButton(
          onPressed: () => test.stop(),
          child: new Icon(
            Icons.stop,
            color: Colors.blue,
            size: 35.0,
          ),
          shape: new CircleBorder(),
          elevation: 2.0,
          fillColor: Colors.white,
          padding: const EdgeInsets.all(15.0),
        ),
      ]);
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Image.asset("assets/Imagenes/logofondonegro.png"),
        button
      ],
    );
  }
}
