import 'package:flutter/foundation.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class IOSProvider with ChangeNotifier {

  String url="http://node-23.zeno.fm/k30mcpek55quv?rj-ttl=5&rj-token=AAABbW56pXrUM4WtRpbxQ2SQNBg_8NaOuTFqDykSzkMAtchbB5D3Hw&rj-tok=AAABb1hBrWMANVR7k9TwaNTLIg";
  bool isLocal=false;
  bool escuchando=false;
  AudioPlayer audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);


  Future<int> play() async {
      await audioPlayer.play(url, isLocal: isLocal, position: null);
      escuchando=true;
      notifyListeners();

  }


  Future<int> stop() async {
    await audioPlayer.stop();
    print("stop");
    escuchando=false;
    notifyListeners();
  }

}