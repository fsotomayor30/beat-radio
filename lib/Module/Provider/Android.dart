
import 'package:flutter/foundation.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class AndroidProvider  with ChangeNotifier {
bool escuchando=false;
String urlMusic;
String activado;

  AndroidProvider(){
    audioStart();
  }

  Future<void> audioStart() async {
    await FlutterRadio.audioStart();
    escuchando=false;
    print('Audio Start OK');
  }

  Future<int> play() async {
    FlutterRadio.play(url: "http://node-23.zeno.fm/k30mcpek55quv?rj-ttl=5&rj-token=AAABbW56pXrUM4WtRpbxQ2SQNBg_8NaOuTFqDykSzkMAtchbB5D3Hw&rj-tok=AAABb1hBrWMANVR7k9TwaNTLIg");
    escuchando=true;
    notifyListeners();

  }

  Future<int> stop() async {
    FlutterRadio.stop();
    escuchando=false;
    notifyListeners();

  }

}


